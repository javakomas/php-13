-- Išrinkime iš darbuotojų lentelės:

-- įrašų kiekį  
SELECT COUNT(*) FROM darbuotojai;
-- atlyginimų sumą
SELECT SUM(salary) FROM darbuotojai;
-- Įrašų skaičių, kurių įdarbinimo tipas=2
SELECT COUNT(*) FROM darbuotojai where idarbinimo_tipas =2;
-- Skirtingus darbuotojų vardus, ir kiek darbuotojų turi tą vardą
SELECT name, count(*) from darbuotojai group by name;
-- Skirtingas vardų ir pavardžių kombinacijas, ir kiek darbuotojų turi tas vardų ir pavardžių kombinacijas
SELECT name, surname, count(*) from darbuotojai group by name, surname; 
-- Vyrų atlyginimų sumą
SELECT sum(salary) from darbuotojai where gender ='vyras';
-- Atlyginimų sumą sugrupuotą pagal lytį
SELECT gender, sum(salary) FROM darbuotojai GROUP BY gender;
-- Atlyginimų sumą sugrupuotą pagal lytį ir pareigos_id
SELECT pareigos-id, gender, sum(salary) FROM darbuotojai GROUP BY gender, pareigos_id;
-- įdarbinimo tipus ir kiek darbuotojų įdarbinta tais tipais
SELECT idarbinimo_tipas, COUNT(*) FROM darbuotojai GROUP BY idarbinimo_tipas;
-- Išvesti skirtingas gimimo dienas ir kiek darbuotojų gimę kurią dieną
SELECT DATE(birthday) birthday_date, COUNT(*) FROM darbuotojai GROUP BY birthday_date;
-- Išvesti atlyginimų sumą. Sugrupuoti pagal išsilavinimą, rodyti tik tas grupes, kuriose atlyginimų suma > 999
SELECT  education, SUM (salary) 
FROM darbuotojai
GROUP BY education
HAVING SUM (salary)>999;
-- Išvesti atlyginimų sumą sugrupuotą pagal įdarbinimo tipą, rodyti tik tas grupes, kuriose daugiau nei 2 žmonės
SELECT SUM(salary), idarbinimo_tipas
FROM darbuotojai
GROUP BY idarbinimo_tipas 
HAVING COUNT(*) > 2




-- Išvesti atlyginimų sumą ir žmonių skaičių grupėms
-- grupuoti pagal įdarbinimo tipą ir pagal išsilavinimą, 
-- rodyti tik tas grupes, kuriose daugiau nei 1 žmogus ir atlyginimų suma > 999,
-- rikiuoti grupes pagal atlyginimų sumą mažėjimo tvarka. 
-- Į rezultatus skaičiuoti tik vyrus, gimusius 2017-09-06
SELECT
    SUM (salary),
    COUNT(*)
    idarbinimo_tipas,
    education
    FROM darbuotojai
    where gender='vyras' AND DATE(birthday) = '2017-09-06'
    GROUP BY idarbinimo tipas, education
    HAVING COUNT(*) > 2 AND SUM (salary)>999
    ORDER BY SUM (salary) DESC 
